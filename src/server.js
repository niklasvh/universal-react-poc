import express from 'express';
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import App from './components/App';
import database from './database';

export default (applyFileServing) => {
    const app = express();
    applyFileServing(app);

    //JSON data request for client rendering(progressive enhancement)
    app.get('/api/*', (req, res, next) => {
        database(req.url).then(data => res.status(data.status).json(data.body)).catch(next);
    });

    //HTML data request rendered on server
    app.get('/*', (req, res, next) => {
        database(req.url).then(data => res.status(data.status).send(renderPage(data.body))).catch(next);
    });

    const renderPage = (data={}) => {
        return '<!DOCTYPE html>' + ReactDOMServer.renderToString(<App {...data} />);
    };

    return app;
};
