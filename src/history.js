import {EventEmitter} from 'events';
import url from 'url';
import fetch from 'isomorphic-fetch';

const route = new EventEmitter();
const NAVIGATION_EVENT = 'navigation';

export const navigateTo = path => {
    const {pathname} = url.parse(path);
    history.pushState(null, null, pathname);
    fetch('/api' + pathname).then(data => route.emit(NAVIGATION_EVENT, data));
};

export const pageChange = callback => {
    route.on(NAVIGATION_EVENT, callback);
};
