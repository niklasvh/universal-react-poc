import url from 'url';

export default path => {
    const {pathname} = url.parse(path);

    switch (pathname) {
        case '/': return Promise.resolve({
            status: 200,
            body: {
                title: 'Universal React Application',
                template: 'homepage',
                content: {
                    hero: {
                        title: 'Universal React',
                        subTitle: 'React application running on the back-end & front-end',
                        buttons: [
                            {href: '/introduction', text: 'Introduction'},
                            {href: 'http://github.com/niklasvh', text: 'Source code'}
                        ]
                    }
                }
            }
        });
        default: return Promise.resolve({status: 404, body: {}});
    }
};
