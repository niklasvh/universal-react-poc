require('babel/register');
const createAppServer = require('./server');
const compiler = require('./compiler');
const webpackDevMiddleware = require('webpack-dev-middleware');
const port = process.env.PORT || 8080;

const server = createAppServer(app => {
    app.use(webpackDevMiddleware(compiler, {
        publicPath: '/public'
    }));
});

server.listen(port, () => console.info('Server (development) listening on port', port));
