import React from 'react';
import Hero from '../components/Hero';

export default page => {
    return (
        <div>
            {page.hero ? <Hero {...page.hero} /> : null}
        </div>
    );
};
