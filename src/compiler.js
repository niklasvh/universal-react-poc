import webpack from 'webpack';
import path from 'path';

export default webpack({
    entry: path.resolve(__dirname, 'client.js'),

    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel'
            }
        ]
    },

    output: {
        filename: 'app.js',
        path: path.resolve(__dirname, '../public')
    }
});
