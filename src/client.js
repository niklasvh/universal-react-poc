import App from './components/App';
import React from 'react';
import ReactDOM from 'react-dom';

const initialContent = window.__reactContent;

ReactDOM.render(<App {...initialContent} />, document);
