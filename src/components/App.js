import React, {Component} from 'react';
import * as templates from '../templates';

const safeJSONStringify = json => JSON.stringify(json).replace(/<\//g, '<\\/').replace(/<!--/g, '\\u003C!--');

const getTemplate = template => templates[template] || templates.error;

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = props;
    }
    componentDidMount() {
        const history = require('../history');
        history.pageChange(data => this.setState(data));
    }
    render() {
        const Template = getTemplate(this.state.template);

        return (
            <html lang="en">
                <head>
                    <title>{this.state.title}</title>
                    <meta charSet="utf-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
                </head>
                <body>
                    <Template {...this.state.content} />
                    <script dangerouslySetInnerHTML={{__html: `window.__reactContent=${safeJSONStringify(this.props)}`}} />
                    <script src={'/public/app.js'} async />
                </body>
            </html>
        );
    }
}
