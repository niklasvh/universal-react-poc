import React from 'react';

const getOrigin = href => {
    var link = document.createElement('a');
    link.href = href;
    link.href = link.href; // IE9
    return link.protocol + link.hostname + link.port;
};

const isSameOrigin = href => getOrigin(href) === getOrigin(location.href);

const navigate = event => {
    const url = event.target.href;
    const history = require('../history');
    if (isSameOrigin(url)) {
        event.preventDefault();
        history.navigateTo(url);
    }
};

export default link => {
    return (
        <a onClick={navigate} {...link}>{link.children}</a>
    );
};
