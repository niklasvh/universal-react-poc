import React from 'react';
import Link from './Link';
import Relay from 'react-relay';

const buttonStyle = {
    fontSize: 12,
    color: '#ececec',
    backgroundColor: '#000'
};

const baseHeading = {
    color: '#ccc',
    textDecoration: 'underline',
    fontWeight: 'bold',
    fontSize: 14
};

const heading1 = Object.assign({}, baseHeading, {
    fontSize: 16
});

const heading2 = Object.assign({}, baseHeading, {
    fontSize: 12
});

const Hero = ({hero}) => {
    return (
        <div>
            <h1 style={heading1}>{hero.title}</h1>
            {hero.subTitle ? <h2 style={heading2}>{hero.subTitle}</h2> : null}
            {hero.buttons.map((button, i) => <Link key={i} href={button.href} style={buttonStyle}>{button.text}</Link>)}
        </div>
    );
};

export default Relay.createContainer(Hero, {
    fragments: {
        hero: () => Relay.QL`
            fragment on Hero {
                title,
                subTitle,
                buttons {
                    href,
                    text
                }
            }
        `
    }
});
